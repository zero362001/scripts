#!/bin/bash

day=""
dayn="$(date +'%u')"

case $dayn in
    "1")
       day="Mon"
       ;;
    "2")
        day="Tue"
        ;;
    "3")
        day="Wed"
        ;;
    "4")
        day="Thu"
        ;;
    "5")
        day="Fri"
        ;;
    "6")
        day="Sat"
        ;;
    "7")
        day="Sun"
        ;;
esac
m_date="$day, $(date +'%h %d | %I:%M %p')"
echo $m_date
